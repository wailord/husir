---
title: "O Děsíři"
date: 2019-08-02T11:04:49+08:00
draft: false
---

<!--# Děsír -->
Děsír je komunitní projekt organizující recesistické akce v Praze.
Cílem těchto akcí je narušit všednost rutiního života a to nejlépe jak organizátorů, tak i okolí.
Aktuálně Děsír funguje na facebookových stránkách [Děsír](https://www.facebook.com/DesirCZ/), kde se organizují akce.

# Tento projekt
Myšlanka při vzniku tohoto projektu je jednoduchá.
S tím jak odchází lidé z Děsíře, odchází i znalosti a zázemí.
Starší webové stránky již nejsou dohledatelné a obsah je tak znehodnocen, čas v horizontu několika málo let.


Cílem této webové stránky (a hlavně pozadí za ní) je udržet inforamce o tom kdo a co byl děsír.
O tom, jaké akce organizoval, co k tomu bylo potřeba a jaké byly nástrahy, tak aby až se jednou najde někdo další, kdo bude chtít pokračovat nemusel začínat od začátku, nebo procházet temné archiválie internetu.

## Jen další webovka?
Tato stránka má k plnohodnotné webovce daleko a to je její hlavní výhoda.
Jedná se jen o pár textových souborů, které by se klidně vešli na jednu disketu.
Tyto soubory využívají pro formátování široce rozšířený [Markdown](https://www.wikiwand.com/en/Markdown) a následně jsou překlopeny do formy webové stránky pomocí [mkdocs](https://www.mkdocs.org).
A to je kouzlo této stránky, jedná se jen o těch pár MB souborů které mají cenu a ty existují nezávisle na jednom konkrétním serveru, nebo člověku.
Navíc tyto soubory jsou spravovány pomocí [gitu](https://www.wikiwand.com/en/Git) což je distribuovaný systém pro sledování změn v textu.
Aktuálně je obsah uložen [https://gitlab.com/wailord/desir](zde), kde je k dispozici ke stažení a úpravě komukoli, kooperace je výtaná.

## Autorské práva
Pro jednodušší sdílení, navrhuji využít [MIT licenci](https://www.wikiwand.com/en/MIT_License) a v tomto projektu vydávat data pod ní.

## Něco navíc
Pro ještě lepší zajištění těchto dat proti ztrátě, bude projekt automaticky zálohován na alternativní služby github a bitbucket.
A také na službu [Internet Archive](https://archive.org)

A navíc v rámci těchto služeb dochází k velmi zajímavé archivaci také, například [archive program](https://archiveprogram.github.com/).


