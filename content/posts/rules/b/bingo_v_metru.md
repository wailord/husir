---
author: "MB"
author_link: "https://gohugo.io/"
title: "Bingo v Metru"
date: 2020-01-01T00:00:00+01:00
lastmod: 2020-01-01T16:45:40+08:00
draft: false
description: "Sample article showcasing basic Markdown syntax and formatting for HTML elements."
show_in_homepage: true
description_as_summary: false
license: ""

tags: [
    "Pravidla",
    "Bingo v metru",
]
categories: [
    "Pravidla",
]

<> featured_image: /images/markdown.png
<> featured_image_preview: ""

comment: true
toc: true
auto_collapse_toc: true
math: true
---

Základní pravidla pro Bingo v metru.
<!--This article offers a sample of basic Markdown syntax that can be used in Hugo content files, also it shows whether basic HTML elements are decorated with CSS in a Hugo theme.-->
<!--more-->

<!--# Bingo v metru-->

## Základní info


## Pravidla


## Iterace

| Datum                                                   | Info   |
| ------------------------------------------------------- | ------ |
| [13. Ledna 2020](../../../2020/01/01_13_bingo_v_metru/) |        |