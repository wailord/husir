---
author: "MB"
author_link: ""
title: "13. 1. Bingo v Metru"
date: 2020-01-13T18:00:00+01:00
lastmod: 2019-10-01T17:55:28+08:00
draft: false
description: "Klasický Děsír, parta lidí, večerní metro a už jen to Bingo."
show_in_homepage: true
description_as_summary: true
license: ""

tags: [
  "Bingo v metru", "Akce"
]
categories: [
  "Akce",
]

comment: true
toc: true
auto_collapse_toc: true
math: false
---

<!--more-->

# Bingo v Metru

|        |                                                          |
| ------ | -------------------------------------------------------- |
| Hra    | [Bingo v Metru](../../../2020/01/bingo_v_metru/)         |
| Datum  | 13. 1. 2020                                              | 
| FB     | [Link](https://www.facebook.com/events/695983470932768/) |
| Org    |                                                          |

## Report

Rozhodli jsme se jezdit větší část trasy metra C tam a zpátky.
Metro C jsme vybrali záměrně, protože je v něm jízda tišší, i tak bylo potřeba dvou až tří lidí, aby předávali informaci dál.
Přes počáteční obavy, že v metru nebude k hnutí, jsme řešili spíše opačný problém a  to, že v metru nebylo moc lidí.
Hrálo se nakonec průběžně, vždy když někdo přistoupil, tak dostal kartičku a hra pokračovala i v okamžiku, kdy někdo vyhrál.
Pro výtěze jsme měli vždy malou cenu, kterých jsme nakonec upotřebyli celkem dost.
Jako obvyklé ceny se užila fidorka, malá čokoláda, nebo kinder vajíčko.
Pro odvážnější byly zařazeny i zajímavější ceny, jako kedlubna.
Za největší úspěch považuji to, že jedna slečna se s námi svezla o stanici dále, než chtěla, jen aby vyhrála.
